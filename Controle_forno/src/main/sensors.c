#include "header.h"
#include <Arduino.h>



/*verificaPorta:  percebe estado do sensor da porta, se for aberto
retorna: retorna 0 se porta aberta e 1 se fechada
recebe: nada
*/
int verificaPorta(){

  if (!digitalRead(sensorPorta)){ // botao apertado
    state = 1;
    return 0;
  }
  if((state == 1)&&(digitalRead(sensorPorta))){ //pq precisa considerar state?
    state = ant_state;
    return 1;
  }

}
