#include <Arduino.h>
#include <SoftwareSerial.h>
#include <Arduino.h>
#include <LiquidCrystal.h>
#include "max6675.h"
#include <SPI.h>


#include "header.h"
#include "operacoes.c"
#include "states.c"
#include "sensors.c"
#include "keypad.c"


//RS485
//SoftwareSerial(rxPin, txPin, inverse_logic)

const int rs485DI = A5;
const int rs485DO = A4;
SoftwareSerial rs485(rs485DO,rs485DI);

//termopar
int thermoDO = A2;
int thermoCS = A1;
int thermoCLK = A0;

int thermoVCC = A3;
int thermoGND = 2;

MAX6675 thermocouple(thermoCLK, thermoCS, thermoDO);

double getTemp = 0.0;
int countLoopTemp=0;
int setcountLoopTemp = 50;

int idxTemp = 0;

//operacao

#define etapas 3

int idx_param = 0;
int confirma = 0;
int controle_confirma = 2; //inicia a variavel em modo sem nada
int etapa_trabalho = 1;  // sempre inicializa com etapa 1
// IMPORTANTE!!!! Verificar a segurança de acionamento.

int etapa_exec = 0; // controla a execu



int g_temperatura = 10; // ganho de 10 em 10 celsius
int g_vapor = 10; // ganho de 10 cento
int g_tempo = 10; // 10 min

int temperatura[etapas];
int vapor[etapas];
int tempo[etapas];

int preaquecimento = 0;

unsigned long tInicial = 0;
unsigned long tAtual = 0;

int cVapor =0;

int hab_acc = 13;

/*sensores
*/
int sensorPorta = 1;




/*teclado
*/
int esq = 9;
int dir = 10;
int enter = 11;
int cancel = 12;


int deb_9 = 0;
int deb_10 = 0;
int deb_11 = 0;
int deb_12 = 0;

int debounce = 4;
/* estados
__________________________________________
| Estados    | Funcao                    |
| 0          | desligado                 |
| 1          | Porta aberta              |
| 2          | Setando parametros        |
| 3          | Executando parametros     |
| 4          | Limpeza                   |


*/
unsigned int state = 0;
unsigned int ant_state = 0; // um estado inicial fora do escopo, pra inicializar
unsigned int cp_state = 0;

/* Declaracoes de pinos
______________________________________
| pino    |   Funcao                  |
| D1      | sensor porta              |
| D2      | GND Thermocouple          |
| D3      | RS LCD                    |
| D4      | Enable LCD                |
| D5      | D4 LCD                    |
| D6      | D5 LCD                    |
| D7      | D6 LCD                    |
| D8      | D7 LCD                    |
| D9      | C1 Keypad                 |
| D10     | C2 Keypad                 |
| D11     | C3 Keypad                 |
| D12     | C4 Keypad                 |
| D13     | hab_acc                   |
| A2      | thermoDO                  |
| A1      | thermoCS                  |
| A0      | thermoCLK                 |
| A3      | VCC Thermocouple          |
| A4      | RxPin-SoftwareSerial      |
| A5      | TxPin-SoftwareSerial      |
|_________|___________________________|


*/

/*status dos atuadores
*/

int stForno = 0;
int stResistencia = 0;
int stPorta = 0;
int stBuzzer = 0;
int stPreaquecido = 0;
int stEnxague = 0;
int stVapor = 0;

/*LCD
*/
const int rs = 3, en = 4, d4 =5 , d5 = 6, d6 = 7, d7 = 8;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);


/*Limpeza - precisara depurar esse codigo
*/
int stLimpeza = 0;
int getTemperaturaLimpeza = 100; //setamento temperatura de limpeza.
int getTempManutencao = 60;
const unsigned long getLimpezaTempo = 1200000; // Aquecimento + vapor
const unsigned long getManutencao = 600000; // é o tempo que põe a temperatura manuntencao
const unsigned long getEnxagueTempo = 600000; // Enxague
const unsigned long getPulsoVapor = 6000; //tempo entre pulso de Vapor
unsigned long limpezaTempo = 0;
unsigned long pulsoLimpezaTempo = 0;
unsigned long enxagueTempo = 0;

/* Debug
*/

#define DEBUG 1

#define TESTE_OP 0

#if TESTE_OP
long testeTempo = 0;
#endif

void setup() {

rs485.begin(9600);

#if DEBUG
Serial.begin(9600);
Serial.println("Placa de controle Guanandi - v0.0");
#endif

lcd.begin(16,2);

pinMode(sensorPorta, INPUT_PULLUP);


pinMode(esq,INPUT_PULLUP);
pinMode(dir,INPUT_PULLUP);
pinMode(enter,INPUT_PULLUP);
pinMode(cancel,INPUT_PULLUP);

pinMode(hab_acc, OUTPUT); //pino que habilita o acc

//Ligacao do MAX6675 hack ladyada
pinMode(thermoGND, OUTPUT);
pinMode(thermoVCC, OUTPUT);
digitalWrite(thermoGND, LOW);
digitalWrite(thermoVCC, HIGH);

getTemp = thermocouple.readCelsius();

#if DEBUG
Serial.println(getTemp);

delay(500);
#endif

}// fim do setup()


void loop() {
  cp_state = state;

  int tecla = verificaTeclado();
  int porta = verificaPorta();

  if(porta == 1){
    cgPorta("d1");
  } else cgPorta("d0");

//se o teclado não estiver sendo usado entao le o termopar, resolver problema do atraso
  if(tecla == 0){
    countLoopTemp++;
    // granulo do loop para acessar a temperatura
    if(countLoopTemp>setcountLoopTemp){
      getTemp = thermocouple.readCelsius();
      countLoopTemp = 0;
    };
  }

  switch (state) {
    case 0: // desligado
      //lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("FORNO BRASCHEFF");
      lcd.setCursor(0,1);
      lcd.print("    @Guanandi");
      digitalWrite(hab_acc, LOW); // desabilita a potencia ACC


      /*Iniciar as variáveris
      */
      idx_param = 0;
      confirma = 0;

      for(int j =0; j<3; j++){
        tempo[j] =0;
        vapor[j]=0;
        temperatura[j]=0;
      }
      //cgForno("e0");

      if (tecla == enter) state = 2 ; // apenas quando o forno estiver em espera liberara o teclado
      if (tecla == esq) state = 4 ; // Limpeza

    break;

    case 1: //porta aberta

      lcd.setCursor(0,0);
      lcd.print("Porta aberta");
      digitalWrite(hab_acc, LOW); // desabilita a potencia ACC

    break;

    case 2: //setando parametros

      int etapa_atual,param_atual;

      if(confirma !=1){
        lcd.setCursor(0,0);
        etapa_atual = idx_param/3;
        lcd.print(etapa_atual+1);
        lcd.print("-Etapa");
      };

      lcd.setCursor(0,1);
      param_atual = idx_param%3;


      if(tecla == cancel){ // se apertar a tecla cancel apaga os dados
         idx_param = 0;
         state = ant_state;
         confirma = 0;
         //rs485.write("e0");
         cgForno("e0");


         for(int j =0; j<3; j++){
           tempo[j] =0;
           vapor[j]=0;
           temperatura[j]=0;
         }
         break; // sai do case, garantir que cada caso eh tratado uma vez em um loop
      }

      if(confirma == 1){
        /*
        SubMaquina de estado - confirma

        confirma = 1 -> entra na submáquina
        controle_confirma =2  estado natural
        controle_confirma = 1 inicia operacao
        controle_confirma = 0 mais etapas.

        */


        if(etapa_atual<2){
          if(controle_confirma ==2){
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Iniciar");
            lcd.setCursor(0,1);
            lcd.print("Mais etapa");
          }


          if(tecla == dir){
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(">Iniciar");
            lcd.setCursor(0,1);
            lcd.print("Mais etapa");
            controle_confirma = 1;
            break;
          }

          if(tecla == esq){
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Iniciar");
            lcd.setCursor(0,1);
            lcd.print(">Mais etapa");
            controle_confirma = 0;
            break;
          }

        }
        else{
          state = 3;
          controle_confirma = 2;
        }

        if ((tecla == enter)&&(controle_confirma == 1)) {
          state = 3;
          controle_confirma = 2;

        }
        if ((tecla == enter)&&(controle_confirma == 0)){
          confirma = 0;
          controle_confirma = 2;
        }
        if(etapa_atual > 2){ // limita a quantidade de etapas.
          state = 3;
          controle_confirma = 2;
        }

        break;
      }

      if((param_atual == 0)&&(confirma == 0)){
        lcd.print("Temperatura:");

        if((tecla == esq)&&(temperatura[etapa_atual]>0)){
          temperatura[etapa_atual] -= g_temperatura;
        }

        if((tecla == dir)&&(temperatura[etapa_atual]<240)){
          temperatura[etapa_atual] += g_temperatura;
        }

        lcd.setCursor(13,1);
        lcd.print("   ");
        lcd.setCursor(13,1);
        lcd.print(temperatura[etapa_atual]);

        if((tecla == enter)&&temperatura[etapa_atual]>0) idx_param++;

        break;
      }

      if((param_atual == 1)&&(confirma == 0)){
        lcd.print("Vapor:          ");

        if((tecla == esq)&&(vapor[etapa_atual]>=0)){
          vapor[etapa_atual] -= g_vapor;
        }

        if((tecla == dir)&&(vapor[etapa_atual]<100)){
          vapor[etapa_atual] += g_vapor;
        }

        lcd.setCursor(7,1);
        lcd.print("   ");
        lcd.setCursor(7,1);
        lcd.print(vapor[etapa_atual]);

        if((tecla == enter)&&vapor[etapa_atual]>=0) idx_param++;

        break;

      }
      if((param_atual == 2)&&(confirma == 0)){
        lcd.print("Tempo:          ");
        if((tecla == esq)&&(tempo[etapa_atual]>0)){
          tempo[etapa_atual] -= g_tempo;
        }

        if((tecla == dir)&&(tempo[etapa_atual]<180)){
          tempo[etapa_atual] += g_tempo;
        }

        lcd.setCursor(7,1);
        lcd.print("   ");
        lcd.setCursor(7,1);
        lcd.print(tempo[etapa_atual]);

        if((tecla == enter) && tempo[etapa_atual]>0){
          idx_param++;
          confirma = 1;
          lcd.clear();

        }
        // aqui sai da setagem de parametros
        break;

      }


    break;

    case 3:

    digitalWrite(hab_acc, HIGH); // habilita a potencia ACC
    if(tecla == cancel){ // se apertar a tecla cancel apaga os dados
       state = 0;
       cVapor = 0;
       preaquecimento = 0;
       cgForno("e0");
      // enviaCmd("e0"); //desliga tudo



       for(int j =0; j<3; j++){
         tempo[j] =0;
         vapor[j]=0;
         temperatura[j]=0;
       }
       break; // sai do case, garantir que cada caso eh tratado uma vez em um loop
    }



      int totalEtapas;
      totalEtapas = idx_param/3; // importante colocar sistema de segurança, caso menor que 1 não iniciar!

      int atualTempe;
      int atualVapor;
      unsigned long atualTempo; // milisegundos , tamanho maximo (2^32 - 1)

      atualTempo = tempo[etapa_exec]*60000; //convertido em milisegundos
      atualTempe = temperatura[etapa_exec];
      atualVapor = vapor[etapa_exec];
      cgForno("e1");
      atualVapor = 610 - 6*atualVapor; // a escala eh invertida -> 100 maximo de vapor 10 eh minimo

      if(preaquecimento == 0){ // preaquece ate temperatura
        lcd.setCursor(0, 0);
        lcd.print("Aquecendo: ");
        lcd.print((int)atualTempe);
        lcd.print("C");
        lcd.setCursor(0,1);
        lcd.print((int)getTemp);
        lcd.print("C");

        cgPreaquecimento("p0"); // o forno ainda não esta aquecido

        if(atualTempe > getTemp){
          cgResistencia("a1");
        }
        else{
          cgResistencia("a0");
          preaquecimento = 1;
          tInicial = millis(); // So inicializa se preaquecer
          lcd.clear();
        }
      }

      else{ //se terminou o preaquecimento == 1

        unsigned long tFalta = millis()-tInicial; // retorna o tempo que falta

        /*#if TESTE_OP
        atualTempo = 10000;
        #endif
        */

        cgPreaquecimento("p1");


        lcd.setCursor(0, 0);
        lcd.print("Faltam ");
        lcd.print((atualTempo-tFalta)/60000);
        lcd.print("m");



        lcd.setCursor(0,1);
        lcd.print((int)getTemp);
        lcd.print("C /");
        lcd.print(" Etapa ");
        lcd.print(etapa_exec+1);

        if (tFalta < atualTempo){ //verifica o tempo de funcionamento



          if(atualTempe > getTemp){
            cgResistencia("a1");
          }
          else{
            cgResistencia("a0");
          }

          //Se o botao Dir for apertado vai ligar o enxague
          if(tecla == dir) cgEnxague("h1");
          else cgEnxague("h0");



          if((10<=atualVapor)&&(atualVapor<600)){ // exclui os valores limites
            if(cVapor >= atualVapor){
              cgVapor("b1");
              cVapor = 0;
            }
          }
          else cVapor = 0; //garantir que nao estoura
          cVapor++;
        }

        else{
          //passa para a proxima etapa_atual
          etapa_exec ++;
          tInicial = millis();

          if(etapa_exec >= totalEtapas){
            cgFim("f1");
            preaquecimento = 0;
            etapa_exec = 0;
            state = 0;
          }
          else{
            lcd.clear();
            cgBuzzer("l1"); // dah um aviso quando mudou de etapa
            #if DEBUG
            Serial.print("prox_etapa");
            #endif
          }
        }
      }
    break;

    case 4: // inicia a Limpeza
    digitalWrite(hab_acc, HIGH); // habilita potencia ACC
    if(tecla == cancel){
       state = 0;
       stLimpeza = 0;
       cgForno("e0");
     }

    switch (stLimpeza) { //sunmaquina de estado limpeza
      case 0: //estado inicial
        lcd.setCursor(0,0);
        lcd.print("Limpeza");
        lcd.setCursor(0,1);
        lcd.print("Confirma?");
        if(tecla == enter){
          stLimpeza = 1;
          lcd.clear();

        }
      break;

      case 1: //ligado

      int TemperaturaLimpeza = 0;
      unsigned long TempoLimpeza;

      lcd.setCursor(0,0);
      lcd.print("Em Limpeza!");
      cgForno("e1");
      if(limpezaTempo == 0) limpezaTempo = millis();

      TempoLimpeza = millis()-limpezaTempo;

      if(TempoLimpeza < getLimpezaTempo){
        if(TempoLimpeza < getManutencao) TemperaturaLimpeza = getTemperaturaLimpeza;
        else TemperaturaLimpeza = getTempManutencao;

        if((int)getTemp<TemperaturaLimpeza) cgResistencia("a1");
        else{
          cgResistencia("a0");
          cgPreaquecimento("p1");
          if(pulsoLimpezaTempo == 0)  pulsoLimpezaTempo = millis();
          if((millis()-pulsoLimpezaTempo) > getPulsoVapor ){
            cgVapor("b1");
            pulsoLimpezaTempo = 0;
          }
        }
      }else{ // atingiu o tempo de Aquecimento
        cgPreaquecimento("p0");
        cgResistencia("a0");
        if(enxagueTempo == 0) enxagueTempo = millis();
        if((millis()-enxagueTempo) < getEnxagueTempo){
          cgEnxague("h1");
        }else{ // acabou o tempo de enxague
          cgEnxague("h0");
          cgForno("e0");
          enxagueTempo = 0;
          limpezaTempo = 0;
          stLimpeza = 0;
          state = 0;
        }
      }
      break;
    } //fim subLimpeza

    break;
  } // fim maquina estado

if(verificaState()){
  lcd.clear();
}




//

delay(30);
} //fim loop


void enviaCmd(char cmd[2]){
  rs485.print(cmd);
  #if DEBUG
  Serial.println(cmd);
  #endif


}
/*Change atuador resistencia
descr: Altera o estado da resistencia, apenas quando for solicitado.
]
*/
void cgResistencia(char cmd[2]){
  if((cmd[0] == 'a')&&(cmd[1] == '1')){
    if(stResistencia == 0){
      stResistencia = 1;
      enviaCmd(cmd);

    }
  }
  if((cmd[0] == 'a')&&(cmd[1] == '0')){
    if(stResistencia == 1){
      stResistencia = 0;
      enviaCmd(cmd);

    }
  }
}

/* change o estado do vapor
*/
void cgVapor(char cmd[2]){
  if((cmd[0]== 'b')&&(cmd[1] == '0')){
    if(stVapor == 1){
      stVapor = 0;
      enviaCmd(cmd);
    }
  }
  if((cmd[0]== 'b')&&(cmd[1] == '1')){
    if(stVapor == 0){
      stVapor = 0; // Ha apenas o acionamento do vapor
      enviaCmd(cmd);
    }
  }
}
/* Change estado do forno, habilitando a placa de potência
*/
void cgForno(char cmd[2]){
  if((cmd[0] == 'e')&&(cmd[1] == '0')){
    if(stForno == 1){
      cgVapor("b0");
      //cgBuzzer("l0");
      cgPreaquecimento("p0");
      stForno = 0;
      enviaCmd(cmd);
      //digitalWrite(hab_acc, LOW); // habilita o rele de acionamento do ACC

    }
  }
  if((cmd[0] == 'e')&&(cmd[1] == '1')){
    if(stForno == 0){
      stForno = 1;
      //digitalWrite(hab_acc, HIGH);
      enviaCmd(cmd);
    }
  }
}
/*
O buzzer soh tem um estado possivel no controle, que eh ligado
*/
void cgBuzzer(char cmd[2]){
  if((cmd[0] == 'l')&&(cmd[1] == '0')){
    if(stBuzzer == 1){
      stBuzzer = 0;
      enviaCmd(cmd);
    }
  }
  if((cmd[0] == 'l')&&(cmd[1] == '1')){
    if(stBuzzer == 0){
      stBuzzer = 0; //para o controle o buzzer ta sempre desligado.
      enviaCmd(cmd);
    }
  }
}
/*O forno soh avisa que o forno ja esta preaquecido
*/
void cgPreaquecimento(char cmd[2]){
  if((cmd[0] == 'p')&&(cmd[1] == '0')){
    if(stPreaquecido == 1){
      stPreaquecido = 0;
      enviaCmd(cmd);
    }
  }
  if((cmd[0] == 'p')&&(cmd[1] == '1')){
    if(stPreaquecido == 0){
      stPreaquecido = 1;
      enviaCmd(cmd);
    }
  }
}

void cgPorta(char cmd[2]){
  if((cmd[0] == 'd')&&(cmd[1] == '0')){
    if(stPorta == 1){
      stPorta = 0;

      enviaCmd(cmd);
    }
  }
  if((cmd[0] == 'd')&&(cmd[1] == '1')){
    if(stPorta == 0){
      stPorta = 1;

      enviaCmd(cmd);
    }
  }
}

void cgEnxague(char cmd[2]){
  if((cmd[0] == 'h')&&(cmd[1] == '0')){
    if(stEnxague == 1){
      stEnxague = 0;
      enviaCmd(cmd);
    }
  }
  if((cmd[0] == 'h')&&(cmd[1] == '1')){
    if(stEnxague == 0){
      stEnxague = 1;
      enviaCmd(cmd);
    }
  }
}

void cgFim(char cmd[2]){
  if((cmd[0] == 'f')&&(cmd[1] == '1')) {
    enviaCmd(cmd);
    stPreaquecido = 0;
    stForno = 0;

  };
}
