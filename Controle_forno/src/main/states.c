#include "header.h"
#include <Arduino.h>


/*verificaEstado:  Se estado tiver mudado, guarda o estado anterior
retorna: 1 se mudar, e 0 senão mudar
recebe: nada
*/

int verificaState(){
  if(cp_state != state){ // Mudou o estado
    ant_state = cp_state;
    cp_state = state;
    return 1;

  }
  else{ //não mudou

    return 0;
  }
}
