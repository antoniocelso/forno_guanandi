//prototypes
int verificaPorta();
int verificaTeclado();
int verificaState();

//funcoes de mudanca de estado do atuador
void enviaCmd(char cmd[2]);
void cgResistencia(char cmd[2]);
void cgForno(char cmd[2]);
void cgBuzzer(char cmd[2]);
void cgPreaquecimento(char cmd[2]);
void cgVapor(char cmd[2]);
void cgPorta(char cmd[2]);
void cgEnxague(char cmd[2]);
void cgFim(char cmd[2]);


extern unsigned int state;
extern unsigned int ant_state;
extern unsigned int cp_state;

//sensor da porta
extern int sensorPorta;

// teclado
extern int esq;
extern int dir;
extern int enter;
extern int cancel;

extern int debounce;
extern int deb_9;
extern int deb_10;
extern int deb_11;
extern int deb_12;

//operacoes
extern int idx_param;
extern int etapa_trabalho;

extern int g_temperatura;
extern int g_vapor;
extern int g_tempo;

//termopar
extern double getTemp;

//status
extern int stForno;
extern int stResistencia;
extern int stPorta;
extern int stbomba;
extern int stVapor;
