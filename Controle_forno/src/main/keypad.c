#include "header.h"
#include <Arduino.h>

/*verificaTeclado:  verifica se alguma tecla foi apertada, faz o debounce via software
retorna: retorna a tecla digitada
recebe: nada
*/


int verificaTeclado(){

  if(!digitalRead(esq)){
    deb_9++;
    deb_10 = 0;
    deb_11 = 0;
    deb_12 = 0;

      if(deb_9 > debounce){
       deb_9 = 0;
       return 9;
      }
    }

   if(!digitalRead(dir)){
    deb_10++;
    deb_9 = 0;
    deb_11 = 0;
    deb_12 = 0;
      if(deb_10 > debounce){
       deb_10 = 0;
       return 10;
      }
    }
  if(!digitalRead(cancel)){
    deb_11++;
    deb_10 = 0;
    deb_9 = 0;
    deb_12 = 0;
      if(deb_11 > debounce){
       deb_11 = 0;
       return 11;
      }
  }

  if(!digitalRead(enter)){
    deb_12++;
    deb_10 = 0;
    deb_11 = 0;
    deb_9 = 0;
      if(deb_12 > debounce){
       deb_12 = 0;
       return 12;
      }
  }


  return 0;

  }
