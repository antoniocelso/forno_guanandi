/*Arquivo para controle de potencia do forno Guanandi
Recebe as informacoes via RS485.
Algoritmo de maquina de estado
*/
#include "header.h"
#include <Arduino.h>
#include <SoftwareSerial.h>

const int motorHorario = 2;
const int motorAnti = 3;
const int resistencia = 4;
const int vapor = 5;
const int enxague = 6;
const int buzzer = 7;
const int sistema = 8;

/*
______________________________________
| pino    |   Funcao                  |
| D2      | motorHorario              |
| D3      | motorAnti                 |
| D4      | resistencia               |
| D5      | vapor                     |
| D6      | enxague                   |
| D7      | buzzer			   |
| D8      | stop/start                |
| A4      | RxPin-SoftwareSerial      |
| A5      | TxPin-SoftwareSerial      |
|_________|___________________________|
*/



const int atuadores = 7; // Pinos de atuadores no sistema


//Definindo o DEBUG
#define DEBUG 1
#define DEBUG_VENT 0

//RS485
SoftwareSerial rs485(A4,A5);


/*Maquina de estado
*/
char rec_ant;
int stResistencia = 0;
int sisLigado = 0;
unsigned long int tempoResistencia = 0; //tempo resistencia lig ou deslig
const  long int setTempoR = 10000; // seta o tempo mínimo que a resistência fica ligada


//Ventilador
int stVentilador = 0; // status da maquina de estado
const unsigned long int setMotorLigado = 120000;
const unsigned long int setMotorDesligado = 30000;
unsigned long int tempoLigado = 0;
unsigned long int tempoDesligado = 0;
int sentidoVentilador = 0;
int ventilador = 0;

//Buzzer
const unsigned long int getBuzzerPeriodo = 1000;
const unsigned long int getBuzzertempo = 30000;
unsigned long int tempoBuzzer = 0;
unsigned long int tempoPeriodo =0;
unsigned long int tempoPeriodoBuzzer =0;
int stBuzzer = 0;
int stPeriodo =0;
int stFim = 0; // quando o cozimento chegar ao fim


//stPorta
int stPorta = 1;

//vapor
const unsigned long int setVapor = 3000;
unsigned long int tempoVapor = 0;
int stVapor = 0;

//lavagem
int stEnxague = 0;

void setup() {
  #if DEBUG
  Serial.begin(9600);
  Serial.println("Placa de potência Guanandi - v0.0");
  #endif

  rs485.begin(9600);

  for(int i=0; i <= atuadores; i++){
    int port = i+2;
    pinMode(port,OUTPUT);
    digitalWrite(port,LOW);
  }
}

void loop() {
  char rec;
    if(rs485.available()){
      rec = rs485.read();
      #if DEBUG
      Serial.print(rec);
      #endif
    }

if(sisLigado == 1){ // o sistema só liga se houver o comando e1

  switch (rec_ant) {

    case 'a': //  estado resistencia
    if(stResistencia == 1){
      if(rec == '0'){
        digitalWrite(resistencia, LOW);
        stResistencia = 0;
      }
    }
    else{ //caso stResistencia == 0
      if(rec == '1'){
        digitalWrite(resistencia, HIGH);
        stResistencia = 1;
      }
    }
    break;


    case 'e': //estado maquina
    if(rec == '0'){
      desligaSistema();
    }

    break;

    case 'b': // estado vapor
    if(rec == '1'){
      stVapor = 1;
    }
    break;

    case 'l': //estado bip
    if(rec == '1') {
      stBuzzer = 1;
      #if DEBUG
      Serial.println("buzzer ligado");
      #endif
    }
    //if(rec == '0') stBuzzer = 0;
    break;

    case 'p': // preaquecimento
    if(rec == '1') stVentilador = 1; //preaqueceu, libera o ventilador.
    break;

    case 'd':
    if(rec == '0') stPorta = 0;
    if(rec == '1') stPorta = 1;
    break;

    case 'h':
    if(rec == '0') stEnxague = 0;
    if(rec == '1') stEnxague = 1;
    break;

    case 'f':
    if(rec == '1') stFim = 1;



  }// termina o switch

  if(stEnxague == 1){
    if(stPorta == 0)digitalWrite(enxague,LOW);
    else digitalWrite(enxague, HIGH);
  }else{
    digitalWrite(enxague, LOW);
  }

// liga o ventilador, mediante a flag
  if(stVentilador == 1){

    if(stPorta == 0){
      digitalWrite(motorHorario, LOW);
      digitalWrite(motorAnti, LOW);
      ventilador = 0;
      tempoDesligado = 0;
      tempoLigado = 0;
    }
    else{
      if(ventilador == 0){
        tempoDesligado = millis();
        ventilador = 5;
        #if DEBUG_VENT
        Serial.println("V = 0");
        #endif
      }

      if((ventilador == 5)&&((millis()-tempoDesligado)> setMotorDesligado)){
        digitalWrite(motorHorario, HIGH); // liga o motor e gira sentido horario
        tempoLigado = millis();
        ventilador = 1;
        #if DEBUG_VENT
        Serial.println("V = 5");
        #endif

      }
      if((ventilador == 1)&&((millis()-tempoLigado)> setMotorLigado)){ // desliga o motor apos setMotorLigado
        digitalWrite(motorHorario, LOW);
        tempoLigado = 0;
        tempoDesligado = millis();
        ventilador = 2;
        #if DEBUG_VENT
        Serial.println("V = 1");
        #endif
      }
      if((ventilador == 2)&&((millis()-tempoDesligado)> setMotorDesligado)){ // liga sentido antihorario apos setMotorDesligado
        digitalWrite(motorAnti, HIGH);
        tempoDesligado = 0;
        tempoLigado = millis();
        ventilador = 3;
        #if DEBUG_VENT
        Serial.println("V = 2");
        #endif

      }
      if((ventilador == 3)&&((millis()-tempoLigado)> setMotorLigado)){ // desliga o motor apos setMotorLigado
        digitalWrite(motorAnti, LOW);
        tempoLigado = 0;
        tempoDesligado = millis();
        ventilador = 4;
        #if DEBUG_VENT
        Serial.println("V = 3");
        #endif

      }
      if((ventilador ==4)&&((millis()-tempoDesligado)> setMotorDesligado)){ // conta o tempo motor apos setMotoDesligado
        tempoDesligado = 0;
        ventilador = 0;
        #if DEBUG_VENT
        Serial.println("V = 4");
        #endif
      }
    }
  } // fim do Ventilador

  //desliga o forno, emite o bipe durante um minuto (?)
  if(stFim == 1){
    //para o caso de buzzer continuo , 2s periodo
    if(tempoPeriodo == 0){// inicia o timer do periodo
      tempoPeriodo = millis();
      if(stPeriodo == 1){
        stPeriodo = 0;
      }
      else stPeriodo = 1;
    } // fim da inicializacao do tempoPeriodo


    if(stPeriodo == 1) digitalWrite(buzzer, HIGH);
    else digitalWrite(buzzer, LOW);

    if(millis()-tempoPeriodo > getBuzzerPeriodo) tempoPeriodo = 0;

    // acionar durante o tempoBuzzer
    if(tempoBuzzer == 0) tempoBuzzer = millis();
    if(((millis()-tempoBuzzer) > getBuzzertempo)||(stPorta == 0)){
      digitalWrite(buzzer, LOW);
      tempoBuzzer = 0;
      desligaSistema();
    }
  } // fim stFim

  // Inicia o Buzzer, toca por um segundo o buzzer
  if(stBuzzer == 1){
    Buzzer();
  } //StBuzzer

if(stVapor == 1){
  if(stPorta == 0){
    digitalWrite(vapor,LOW);
    tempoVapor = 0;
  }else{
    if(tempoVapor == 0 ){
      tempoVapor = millis();
      digitalWrite(vapor, HIGH);
    }else{
      if((millis()-tempoVapor)> setVapor){
        digitalWrite(vapor,LOW);
        stVapor = 0;
        tempoVapor = 0;
      }
    }
  }

}else digitalWrite(vapor,LOW); // por seguranca
//fim do vapor
} // sistema ligado

else{ // sistema de seguranca desliga tudo ate 'e1'
for(int i=0; i <= atuadores; i++){
  int port = i+2;
  digitalWrite(port,LOW);
}

  if((rec_ant == 'e')&&(rec == '1')){ // liga o sistema 'e1'
    sisLigado = 1;
    digitalWrite(sistema, HIGH);
  }
}

if(isAlphaNumeric(rec))rec_ant = rec;

delay(30);
} //fim loop

/*Desliga o sistema, não recebe nada, não retorna nada
*/
void desligaSistema(){
  sisLigado = 0; // desliga o sistema
  stVentilador = 0; // desliga o ventilador precisa ver se aqui eh o melhor lugar
  stResistencia = 0;
  stFim = 0;
  stVentilador = 0;
  stVapor =0;

}

void Buzzer(){
  digitalWrite(buzzer,HIGH);
  
  if (tempoPeriodoBuzzer == 0) tempoPeriodoBuzzer = millis();

  if(millis()-tempoPeriodoBuzzer > getBuzzerPeriodo){
    tempoPeriodoBuzzer = 0;
    digitalWrite(buzzer, LOW);
    stBuzzer = 0;
  }
}
