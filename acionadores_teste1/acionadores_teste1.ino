
const int motorHorario = 2;
const int motorAnti = 3;
const int resistencia = 4;
const int vapor = 5;
const int bomba = 6;

void setup() {
  
  for(int port=2; port<7;port++){
    pinMode(port,OUTPUT);
  }

}

void loop() {
 for(int port=2; port<7;port++){
    digitalWrite(port,HIGH);
    delay(100);
    digitalWrite(port, LOW);
  } 
  
}
