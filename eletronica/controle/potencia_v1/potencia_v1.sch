EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:8_relay_module
LIBS:modrs-485
LIBS:potencia_v1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Garagino J1
U 1 1 5C82C644
P 5550 5150
F 0 "J1" H 5550 5850 50  0000 C CNN
F 1 "Garagino" H 5600 4300 50  0000 C CNN
F 2 "" H 5150 5150 50  0001 C CNN
F 3 "" H 5150 5150 50  0001 C CNN
	1    5550 5150
	1    0    0    -1  
$EndComp
$Comp
L 8_relay_module K1
U 1 1 5C83DA06
P 5500 2900
F 0 "K1" H 3200 2900 50  0000 L CNN
F 1 "8_relay_module" H 7550 2900 50  0000 L CNN
F 2 "SRD-5VDC-SL-C" H 7950 2750 50  0001 C CNN
F 3 "" H 5450 2500 50  0001 C CNN
	1    5500 2900
	1    0    0    -1  
$EndComp
$Comp
L ModRS-485 U1
U 1 1 5C83DA71
P 8450 5050
F 0 "U1" H 8450 5500 60  0000 C CNN
F 1 "ModRS-485" H 8450 4650 60  0000 C CNN
F 2 "" H 8450 5150 60  0001 C CNN
F 3 "" H 8450 5150 60  0001 C CNN
	1    8450 5050
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR3
U 1 1 5C83DBA2
P 4800 5150
F 0 "#PWR3" H 4800 5000 50  0001 C CNN
F 1 "VCC" H 4800 5300 50  0000 C CNN
F 2 "" H 4800 5150 50  0001 C CNN
F 3 "" H 4800 5150 50  0001 C CNN
	1    4800 5150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR4
U 1 1 5C83DBBC
P 4800 5250
F 0 "#PWR4" H 4800 5000 50  0001 C CNN
F 1 "GND" H 4800 5100 50  0000 C CNN
F 2 "" H 4800 5250 50  0001 C CNN
F 3 "" H 4800 5250 50  0001 C CNN
	1    4800 5250
	1    0    0    -1  
$EndComp
NoConn ~ 4950 5350
NoConn ~ 4950 5450
$Comp
L GND #PWR8
U 1 1 5C83DBF8
P 10000 5350
F 0 "#PWR8" H 10000 5100 50  0001 C CNN
F 1 "GND" H 10000 5200 50  0000 C CNN
F 2 "" H 10000 5350 50  0001 C CNN
F 3 "" H 10000 5350 50  0001 C CNN
	1    10000 5350
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR7
U 1 1 5C83DC1A
P 9950 4600
F 0 "#PWR7" H 9950 4450 50  0001 C CNN
F 1 "VCC" H 9950 4750 50  0000 C CNN
F 2 "" H 9950 4600 50  0001 C CNN
F 3 "" H 9950 4600 50  0001 C CNN
	1    9950 4600
	1    0    0    -1  
$EndComp
Text GLabel 9850 4950 2    60   Input ~ 0
B
Text GLabel 9850 5100 2    60   Input ~ 0
A
$Comp
L GND #PWR6
U 1 1 5C83DD4A
P 6800 5050
F 0 "#PWR6" H 6800 4800 50  0001 C CNN
F 1 "GND" H 6800 4900 50  0000 C CNN
F 2 "" H 6800 5050 50  0001 C CNN
F 3 "" H 6800 5050 50  0001 C CNN
	1    6800 5050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR2
U 1 1 5C83DE54
P 4800 3600
F 0 "#PWR2" H 4800 3350 50  0001 C CNN
F 1 "GND" H 4800 3450 50  0000 C CNN
F 2 "" H 4800 3600 50  0001 C CNN
F 3 "" H 4800 3600 50  0001 C CNN
	1    4800 3600
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR5
U 1 1 5C83DE98
P 6150 3500
F 0 "#PWR5" H 6150 3350 50  0001 C CNN
F 1 "VCC" H 6150 3650 50  0000 C CNN
F 2 "" H 6150 3500 50  0001 C CNN
F 3 "" H 6150 3500 50  0001 C CNN
	1    6150 3500
	1    0    0    -1  
$EndComp
NoConn ~ 4950 4550
NoConn ~ 4950 4650
NoConn ~ 4950 4750
NoConn ~ 6200 5850
NoConn ~ 6200 5750
NoConn ~ 6200 5650
NoConn ~ 6200 5550
NoConn ~ 6200 5450
NoConn ~ 6200 5350
NoConn ~ 6200 5250
NoConn ~ 6200 5150
NoConn ~ 6200 5050
NoConn ~ 6200 4950
NoConn ~ 6200 4850
NoConn ~ 6200 4750
$Comp
L +24V #PWR1
U 1 1 5C83E313
P 2550 2100
F 0 "#PWR1" H 2550 1950 50  0001 C CNN
F 1 "+24V" H 2550 2240 50  0000 C CNN
F 2 "" H 2550 2100 50  0001 C CNN
F 3 "" H 2550 2100 50  0001 C CNN
	1    2550 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5150 4950 5150
Wire Wire Line
	4800 5250 4950 5250
Wire Wire Line
	10000 5350 10000 5250
Wire Wire Line
	10000 5250 9850 5250
Wire Wire Line
	9850 4800 9950 4800
Wire Wire Line
	9950 4800 9950 4600
Wire Wire Line
	6200 4650 7100 4650
Wire Wire Line
	7100 4650 7100 4800
Wire Wire Line
	6200 4550 6650 4550
Wire Wire Line
	6650 4550 6650 5250
Wire Wire Line
	6650 5250 7100 5250
Wire Wire Line
	7100 5100 6950 5100
Wire Wire Line
	6950 5100 6950 5050
Wire Wire Line
	6950 5050 6950 4950
Wire Wire Line
	6950 4950 7100 4950
Wire Wire Line
	6950 5050 6800 5050
Connection ~ 6950 5050
Wire Wire Line
	4800 3600 4800 3500
Wire Wire Line
	4800 3500 5150 3500
Wire Wire Line
	6150 3500 6150 3650
Wire Wire Line
	6150 3650 5950 3650
Wire Wire Line
	5950 3650 5950 3500
Wire Wire Line
	4950 4850 4850 4850
Wire Wire Line
	4850 4850 4850 4400
Wire Wire Line
	4850 4400 5250 4400
Wire Wire Line
	5250 4400 5250 3500
Wire Wire Line
	4950 4950 4700 4950
Wire Wire Line
	4700 4950 4700 4250
Wire Wire Line
	4700 4250 5350 4250
Wire Wire Line
	5350 4250 5350 3500
Wire Wire Line
	4950 5050 4550 5050
Wire Wire Line
	4550 5050 4550 4100
Wire Wire Line
	4550 4100 5450 4100
Wire Wire Line
	5450 4100 5450 3500
Wire Wire Line
	4950 5550 4450 5550
Wire Wire Line
	4450 5550 4450 4050
Wire Wire Line
	4450 4050 5550 4050
Wire Wire Line
	5550 4050 5550 3500
Wire Wire Line
	4950 5650 4300 5650
Wire Wire Line
	4300 5650 4300 4000
Wire Wire Line
	4300 4000 5650 4000
Wire Wire Line
	5650 4000 5650 3500
Wire Wire Line
	4950 5750 4150 5750
Wire Wire Line
	4150 5750 4150 3950
Wire Wire Line
	4150 3950 5750 3950
Wire Wire Line
	5750 3950 5750 3500
Wire Wire Line
	4950 5850 4100 5850
Wire Wire Line
	4100 5850 4100 3850
Wire Wire Line
	4100 3850 5850 3850
Wire Wire Line
	5850 3850 5850 3500
Text Label 3850 2300 1    60   ~ 0
Moto_Horário
Text Label 4350 2300 1    60   ~ 0
Motor_AntiHorário
Text Label 4850 2300 1    60   ~ 0
Resistência
Text Label 6350 2300 1    60   ~ 0
Vapor
Text Label 6850 2300 1    60   ~ 0
Enxague
Text Label 7350 2300 1    60   ~ 0
Buzzer
NoConn ~ 6550 2300
NoConn ~ 6050 2300
NoConn ~ 5550 2300
NoConn ~ 5050 2300
NoConn ~ 4550 2300
NoConn ~ 4050 2300
NoConn ~ 3550 2300
Wire Wire Line
	5200 2100 5200 2300
Connection ~ 5200 2100
Wire Wire Line
	4700 2300 4700 2100
Connection ~ 4700 2100
Wire Wire Line
	4200 2300 4200 2100
Connection ~ 4200 2100
Wire Wire Line
	3700 2300 3700 2100
Connection ~ 3700 2100
Text Label 5350 2300 1    60   ~ 0
Stop
Wire Wire Line
	2550 2100 3700 2100
Wire Wire Line
	3700 2100 4200 2100
Wire Wire Line
	4200 2100 4700 2100
Wire Wire Line
	4700 2100 5200 2100
NoConn ~ 5700 2300
NoConn ~ 5850 2300
$Comp
L AC #PWR?
U 1 1 5C83ECBC
P 8050 2000
F 0 "#PWR?" H 8050 1900 50  0001 C CNN
F 1 "AC" H 8050 2250 50  0000 C CNN
F 2 "" H 8050 2000 50  0001 C CNN
F 3 "" H 8050 2000 50  0001 C CNN
	1    8050 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 2000 7200 2000
Wire Wire Line
	7200 2000 6700 2000
Wire Wire Line
	6700 2000 6200 2000
Wire Wire Line
	6200 2000 6200 2300
Wire Wire Line
	6700 2300 6700 2000
Connection ~ 6700 2000
Wire Wire Line
	7200 2300 7200 2000
Connection ~ 7200 2000
NoConn ~ 7050 2300
$EndSCHEMATC
